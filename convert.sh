script_dir=`dirname "$0"`

command -v mogrify >/dev/null 2>&1 || { echo >&2 "Imagemagick is required but not installed. Aborting"; exit 1; }
command -v pandoc >/dev/null 2>&1 || { echo >&2 "Pandoc is required but not installed. Aborting"; exit 1; }
command -v htmldoc >/dev/null 2>&1 || { echo >&2 "HTMLDOC is required but not installed. Aborting"; exit 1; }

INFILE=$1
if [ ! -f $INFILE ]; then
    echo "File not found. Aborting."
    exit 1
fi

TITLE=$(head -n 1 $INFILE)
tail -n +2 $INFILE | pandoc -5 -o ${INFILE}.temp.html
sed "/<!--CONTENT-->/{
    s/<!--CONTENT-->//g
    r ${INFILE}.temp.html
}" "$script_dir/template.html" |
sed "s/<!--TITLE-->/$TITLE/g" |
iconv -f UTF8 -t ISO8859-1 -c > ${INFILE}.temp_.html

if [ -d resources ]; then
    mogrify -quality 100 -format jpeg -fill "#FFFFFF" -opaque none resources/*.png
fi

htmldoc ${INFILE}.temp_.html --charset iso-8859-1 --toctitle Inhaltsverzeichnis --numbered -f ${INFILE}.pdf --webpage
rm ${INFILE}.temp.html
rm ${INFILE}.temp_.html
